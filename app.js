const express = require("express");
const app = express();
const mysql = require("mysql");

/*DB connection*/
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'nodemysqlresdis'
});
/*db connect*/
db.connect((err) => {
    if(err){
        throw err;
    }
    console.log("Mysql connected...");
})

/*create db*/
app.get('/createdb',(req,res)=>{
    let sql = 'CREATE DATABASE nodemysqlresdis';
    db.query(sql,(err,result)=>{
        if(err) throw err;
        res.send('Database created....');
    });
});

/*create table*/
app.get('/createPostsTable',(req,res)=>{
    let sql = "CREATE TABLE `nodemysqlresdis`.`users` ( `pointer` INT(11) NOT NULL AUTO_INCREMENT , `username` VARCHAR(200) NOT NULL , `full_Name` VARCHAR(200) NOT NULL , `phone_number` INT(200) NOT NULL , `address` TEXT NOT NULL,`active` TINYINT(4) NOT NULL, PRIMARY KEY (`pointer`)) ENGINE = InnoDB";
    db.query(sql,(err,result)=>{
        if(err) throw err;
        console.log(result);
        res.send('Database Table created....');
    });
});

/*Insert User*/
app.get('/addUsers',(req,res)=>{
    let users = [
        {username:'rakib10cuet',full_Name:'Rezwoan Ahmed Rakib',phone_number: '01671358095',address: 'Cumilla',active: 0},
        {username:'rashedul',full_Name:'Rashedul Islam',phone_number: '01671359225',address: 'Rangpur',active: 1},
        {username:'saiful',full_Name:'Saiful Islam',phone_number: '01671359224',address: 'Dhaka',active: 1}
    ];
    let sql = "INSERT INTO `users` SET ?";
    users.forEach(function(value) {
        console.log(value.active)
        db.query(sql,value,(err, result)=>{
            if(err) throw err;
        });
    })
    res.send('User All added....');
});

/*for redis connection*/
const redis = require("redis");
const client = redis.createClient();

/*using manual redis configuration*/
// const redisConf = {
//     host: 'localhost',
//     port: '6379',
//     pass: ''
// }
// const client = redis.createClient(redisConf);
client.on("error",(error)=>{
    console.log("Redis is Off");
});
client.on("connect",(error)=>{
    console.log("Redis Connection Established");
});

/*Select Users*/
app.get('/getUser/:id',(req,res)=>{
    client.get("userData"+req.params.id,(err,result)=>{
        let redisData = JSON.parse(result);
        let timeout = 10;
        if(!redisData){
            console.log("db");
            let sql = `SELECT * FROM users WHERE pointer = ${req.params.id}`;
            db.query(sql,(err, result_redis)=>{
                if(err) throw err;
                if(result_redis[0].active === 1){
                    let obj = JSON.stringify(result_redis[0])
                    client.set("userData"+result_redis[0].pointer,obj,"EX",timeout,(err)=>{
                        if(err) throw err;
                    });
                }
                res.send(result_redis[0]);
            });

        }else{
            console.log('redis');
            res.send(redisData);
        }
    });
});

/*Update Users*/
app.get('/updateUser/:id',(req,res)=>{
    let changeColumnName = "username";
    let changeColumnValue = "rashedulApsis";

    let sql = `UPDATE users SET ${changeColumnName} = '${changeColumnValue}' WHERE pointer = ${req.params.id}`;
    db.query(sql,(err)=>{
        if(err) throw err;
        client.exists("userData"+req.params.id,(err,redisExistsResult)=>{
            if(err) throw err;
            if(redisExistsResult === 1){
                client.DEL("userData"+req.params.id,(err)=>{
                    if(err) throw err;
                });
            }
        });
    });
    let sql1 = `SELECT * FROM users WHERE pointer = ${req.params.id}`;
    db.query(sql1,(err, dbResult)=>{
        if(err) throw err;
        if(dbResult[0].active === 1){
            let obj = JSON.stringify(dbResult[0])
            client.set("userData"+dbResult[0].pointer,obj,"EX",60*60,(err)=>{
                if(err) throw err;
            });
        }
        res.send(dbResult[0]);
    });
});

/*For redis test*/
app.get('/redisStringInsertGet',(req,res)=>{
    /*string using set-get*/
    let username = 'Rezwoan Ahmed Rakib';
    // let timeout = 60 * 60 * 24;
    let timeout = 60 * 5;
    client.set('KeyForString',username);
    client.expire('KeyForString', timeout);

    // client.set('username', username, 'EX', timeout, (err,result)=>{
    //     if(err) throw err;
    //     console.log("Insert Redis");
    // });

    client.get("KeyForString",(err,result)=>{
        if(err) throw err;
        let redisData = (result === null) ? "No available in Redis" : result;
        res.send(redisData);
    });
});

app.get('/redisStringGet',(req,res)=>{
    /*string using set-get*/
    client.get("KeyForString",(err,result)=>{
        if(err) throw err;
        let redisData = (result === null) ? "No available in Redis" : result;
        res.send(redisData);
    });
});

app.get('/redisStringDeleteGet',(req,res)=>{
    /*string delete from redis*/
    client.DEL('KeyForString',(err,result)=>{
        if(err) throw err;
    });
    client.get("KeyForString",(err,result)=>{
        if(err) throw err;
        let redisData = (result === null) ? "Deleted data from redis" : result;
        res.send(redisData);
    });
});

app.get('/redisObjectInsertGet',(req,res)=>{
    /*object using set-get*/
    let obj = JSON.stringify({name:"rakib", mobile:'016712222'})
    client.set('KeyForObject',obj);
    client.get("KeyForObject",(err,result)=>{
        let redisData = (result === null) ? "No available in Redis" : JSON.parse(result);
        res.send(redisData);
    // APPEND, DECR, DECRBY, GET, GETDEL, GETEX, GETRANGE, GETSET, INCR, INCRBY, INCRBYFLOAT, MGET, MSET, MSETNX, PSETEX, SET, SETEX, SETNX, SETRANGE, STRALGO, STRLEN
    });
});

app.get('/redisObjectDeleteGet',(req,res)=>{
    /*object delete from redis*/
    client.DEL('KeyForObject',(err,result)=>{
        if(err) throw err;
        console.log(result);
    });
    client.get("KeyForObject",(err,result)=>{
        console.log(result)
        let redisData = (result === null) ? "Deleted Data" : JSON.parse(result);
        res.send(redisData);
    });
});

app.get('/redisHashInsertGet',(req,res)=>{
    /*Hash using HSET-HGET*/
    let hashValue = "generated Hash";
    client.HSET('myHash','field1',hashValue);
    client.HGET("myHash","field1",(err,result)=>{
        let redisData = (result === null) ? "No available in Redis" : result;
        res.send(redisData);
    });
    // HDEL,HEXISTSHGET, HGETALL, HINCRBY, HINCRBYFLOAT, HKEYS, HLEN, HMGET, HMSET, HRANDFIELD, HSCAN, HSET, HSETNX, HSTRLEN, HVALS,
});

app.get('/redisHashDeleteGet',(req,res)=>{
    /*object delete from redis*/
    client.HDEL('myHash','field1',(err,result)=>{
        if(err) throw err;
        console.log(result);
    });
    client.HGET("myHash","field1",(err,result)=>{
        let redisData = (result === null) ? "No available in Redis" : result;
        res.send(redisData);
    });
});

app.get('/ShowAllRedisKeys',(req,res)=>{
    /*See ALl keys exist in Redis*/
    client.keys('*', function (err, keys) {
        if (err) return console.log(err);
        for(var i = 0, len = keys.length; i < len; i++) {
            console.log(keys[i]);
        }
        res.send(keys);
    });
});

app.get('/DeleteAllRedisKeys',(req,res)=>{
    /*Deleting All keys from redis(flushdb/flushall)*/
    client.flushall( function (err, succeeded) {
        res.send(succeeded);
    });
});

app.get('/',(req,res)=>{
    res.send("Simple Redis Practices");
});

const port = 3000;
app.listen(port,()=>{
    console.log(`Server Started on Port ${port}`);
});